let data = module.require("./js_drill_5");
let dataBase = data.dataSet;

function groupByStatus(data) {
  // check the given data correct or not
  if (Array.isArray(data)) {
    let project = [];
    // get the project details
    data.forEach((element) => {
      project.push(element.projects);
    });
    let inOne = project.flat();
    // grouping by using reduce method
    const groupBy = inOne.reduce((acc, value) => {
      if (!acc[value.status]) {
        acc[value.status] = [value.name];
      }
      acc[value.status].push(value.name);
      return acc;
    }, {});
    return groupBy;
  } else {
    return "Data Insufficeient";
  }
}
// Export the code
module.exports = { groupByStatus };
