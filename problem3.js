
function uniqueLanguages(data) {
    // check the given the data correct or not
  if (Array.isArray(data)) {
    // get the languages in the data set
    let getLanguages = data
      .reduce((acc, Value) => {
        if (Value.languages) {
          acc.push(Value.languages);
        }
        return acc;
      }, [])
      .flat();
      // get unique values 
    let set = new Set(getLanguages);
    return Array.from(set);
  } else {
    return "Data Insufficeint";
  }
}
// Export the code 
module.exports = {uniqueLanguages}